# SIGECA #

Sistema de Gestión de solicitudes de prestamos a la caja de ahorro de la empresa venezolana de cementos s.a.c.a

El cual incluye desarrollo de portal web informativo y aplicacion web.

Este sistema le permite a la junta administrativa tomar decisiones rapidamente gracias a las **estadisticas graficas** que emite.

le permite al asociado de la caja de ahorro solicitar prestamos desde su casa con tan solo poseer un smarthphone, laptop, tablet, conectado a internet, esto es gracias a que se desarrollo pensando en tecnologia movil por lo tanto es adaptable y funcional en cualquier de los dispositivos antes mencionados.

ademas de esto le permite conocer su estado de cuenta y estar tranquilo con los saldos que posee gracias a que la informacion es clara y veraz.

Este sistema de gestion esta siendo desarrollado utilizando las tecnologias mas actuales como **PHP (Framework Codeigniter), MySQL, HTML5, AngularJS, Bootstrap.**

Actualmente en desarrollo con un avance de 90%


capturas de pantalla
![capture5.JPG](https://bitbucket.org/repo/Bjergj/images/1583066849-capture5.JPG)


![Capture.JPG](https://bitbucket.org/repo/Bjergj/images/2004461615-Capture.JPG)


![Capture2.JPG](https://bitbucket.org/repo/Bjergj/images/2041118102-Capture2.JPG)