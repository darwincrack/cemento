<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('form_validation'));

	}
	
	public function set_contact(){
		
		if($this->input->post("nombre")&&$this->input->post("email")&&$this->input->post("mensaje"))
		{
			
			    $this->form_validation->set_rules('nombre', 'password', 'trim|required|xss_clean');
            	$this->form_validation->set_rules('email', 'Email',   'trim|required|valid_email|xss_clean');
            	$this->form_validation->set_rules('mensaje', 'mensaje', 'trim|required|xss_clean');
            	
            	if($this->form_validation->run() == false){
                	echo json_encode(array("respuesta" => "incomplete_form"));
            	}else{
			
					$this->load->model('contacto_model'); 
					$nombre	=	$this->input->post("nombre");
					$email	=	$this->input->post("email");
					$mensaje=	$this->input->post("mensaje");				
					
					$data=$this->contacto_model->set_contact($nombre,$email,$mensaje);
					
					if($data>=1){
						  echo json_encode(array("respuesta" => "success"));
					}
					else{
						 echo json_encode(array("respuesta" => "failed"));
					}
				
				}
		
		}
		else
		{
			 echo json_encode(array("respuesta" => "incomplete_form"));
		}
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */