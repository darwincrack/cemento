<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csv extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();

	}
	
	
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
				
		else
		{
		$this->template->add_js('js/angular.min.js');
		$this->template->add_js('js/app/controller/csv.js');	
		$this->load->library('ftp');
		$config['hostname'] = 'localhost';
		$config['username'] = 'cav';
		$config['password'] = '12345';
		$config['debug'] = TRUE;
		$this->ftp->connect($config);
		$data["list"] = $this->ftp->list_files('/');

	
		//print_r($list);
		$this->ftp->close();
			
			$this->template->write('title', 'Subir CSV', TRUE);
			$this->template->write_view('menu', $this->ion_auth->getter_menu());
		    $this->template->write_view('content', 'csv_upload_view',$data,TRUE);	
			$this->template->render();
			
		}
	}
	
	
	public function set_csv(){
		
		ini_set('memory_limit', '-1');
		$this->load->model('csv_model'); 
		$this->load->library('csvreader');
		
		//obtener el nombre de los csv a buscar en el ftp		
		$data=$this->csv_model->get_name_csv();
		foreach ($data as $row)
		{
			$table_database= trim($row["table_database"]);
			$name_csv= trim($row["nombre"]);
			$id_csv=$row["id"];
			
			$result_csv=$this->csvreader->parse_file("ftp://cav:12345@localhost/$name_csv");
			
			switch ($table_database) {
    			case "edo_cuenta":
    			$this->csv_model->set_edo_cuenta($id_csv,$table_database,$result_csv);
        			break;
    			case 'concepto':
					$this->csv_model->set_concept($id_csv,$table_database,$result_csv);
        			break;
    			case "detalles_mov":
        			$this->csv_model->set_detalles_mov($id_csv,$table_database,$result_csv);
        			break;
        		case "movimiento":
        			$this->csv_model->set_mov($id_csv,$table_database,$result_csv);
        			break;
}
					
		}
		
		
		
		
		
		

	}
	
	function detectarextfile($name_file)
	{

		$trozos = explode(".", $name_file); 
		$extension = end($trozos); 
		// mostramos la extensi�n del archivo
		if($extension=="csv")
		{
			return true;
		}
			return false;
		 
	
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */