<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_excel extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->database();
	    $this->load->model('Reportes_model');
	}
	
	
	
	function lista_socios()
	{
		$data=$this->Reportes_model->get_list_socios();
		
		
			if(empty($data))
			{ 
				$data['error']='No hay Socios Activos';
				$this->template->write_view('content', 'no_encontrado',$data,TRUE);
			}
			else
			{
					//load our new PHPExcel library
					$this->load->library('excel');
					//activate worksheet number 1
					$this->excel->setActiveSheetIndex(0);
					//name the worksheet
					$this->excel->getActiveSheet()->setTitle('test worksheet');
					//titulos
					$this->excel->getActiveSheet()->SetCellValue("A1",'CEDULA');
					$this->excel->getActiveSheet()->SetCellValue("B1",'NOMBRE');
					$this->excel->getActiveSheet()->SetCellValue("C1",'EMAIL');
					$this->excel->getActiveSheet()->SetCellValue("D1",'TELEFONO');
					
					
					//ajustamos el ancho de las columnas al ancho del texto
					$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					
					$i=2;
					foreach($data as $result )
					{
						
						$this->excel->getActiveSheet()->SetCellValue("A".$i, $result['cedula']);
						$this->excel->getActiveSheet()->SetCellValue("B".$i, $result['nombre']);
						$this->excel->getActiveSheet()->SetCellValue("C".$i, $result['email']);
						$this->excel->getActiveSheet()->SetCellValue("D".$i, $result['telefono']);
						
						$i++;
					}
					
				 //set aligment to center for that merged cell (A1 to D1)
				  $this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				  
				   
				  $filename='lista_socios.xlsx'; //save our workbook as this file name
				  header('Content-Type: application/vnd.ms-excel'); //mime type
				  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				  header('Cache-Control: max-age=0'); //no cache
							   
				  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				  //if you want to save it as .XLSX Excel 2007 format
				  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				  //force user to download the Excel file without writing it to server's HD
				  $objWriter->save('php://output');
			}
		
		}
		
		
		
		
	public function lista_prestamo_tipo($fecha_inicial,$fecha_final,$id_tipo_prestamo)
	{
		
		$fecha_inicial	=	str_replace("_","/",$fecha_inicial);
		$fecha_final	=	str_replace("_","/",$fecha_final);
		
		$data=$this->Reportes_model->get_list_prestamo_tipo($fecha_inicial,$fecha_final,$id_tipo_prestamo);
		
			if(empty($data))
			{ 
				$data['error']='No se encontraron registros en la base de datos, intente de nuevo';
				$this->template->write_view('content', 'no_encontrado',$data,TRUE);
				
			}
			else
			{
					//load our new PHPExcel library
					$this->load->library('excel');
					//activate worksheet number 1
					$this->excel->setActiveSheetIndex(0);
					//name the worksheet
					$this->excel->getActiveSheet()->setTitle('test worksheet');
					//titulos
					$this->excel->getActiveSheet()->SetCellValue("A1",'TIPO DE PRESTAMO');
					$this->excel->getActiveSheet()->SetCellValue("B1",'C.I SOCIO');
					$this->excel->getActiveSheet()->SetCellValue("C1",'NOMBRE SOCIO');
					$this->excel->getActiveSheet()->SetCellValue("D1",'FECHA DE SOLICITUD');
					$this->excel->getActiveSheet()->SetCellValue("E1",'MONTO');
					$this->excel->getActiveSheet()->SetCellValue("F1",'ESTATUS');
					
					//ajustamos el ancho de las columnas al ancho del texto
					$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
					
					$i=2;
					foreach($data as $result )
					{
						
						$this->excel->getActiveSheet()->SetCellValue("A".$i, $result['descripcion']);
						$this->excel->getActiveSheet()->SetCellValue("B".$i, $result['cedula']);
						$this->excel->getActiveSheet()->SetCellValue("C".$i, $result['nombre']);
						$this->excel->getActiveSheet()->SetCellValue("D".$i, $result['fecha_creado']);
					    $this->excel->getActiveSheet()->SetCellValue("E".$i, $result['monto']);
						$this->excel->getActiveSheet()->SetCellValue("F".$i, $result['status']);
						
						$i++;
					}
					
				 //set aligment to center for that merged cell (A1 to D1)
				  $this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				  
				   
				  $filename='tipo_prestamos.xlsx'; //save our workbook as this file name
				  header('Content-Type: application/vnd.ms-excel'); //mime type
				  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				  header('Cache-Control: max-age=0'); //no cache
							   
				  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				  //if you want to save it as .XLSX Excel 2007 format
				  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				  //force user to download the Excel file without writing it to server's HD
				  $objWriter->save('php://output');
				
			}
			
		
	}
	
		
		
	public function lista_prestamo_estatus($fecha_inicial,$fecha_final,$id_estatus_prestamo)
	{
		
		$fecha_inicial	=	str_replace("_","/",$fecha_inicial);
		$fecha_final	=	str_replace("_","/",$fecha_final);
		
		$data=$this->Reportes_model->get_list_prestamo_estatus($fecha_inicial,$fecha_final,$id_estatus_prestamo);
		
			if(empty($data))
			{ 
				$data['error']='No se encontraron registros en la base de datos, intente de nuevo';
				$this->template->write_view('content', 'no_encontrado',$data,TRUE);
				
			}
			else
			{
					//load our new PHPExcel library
					$this->load->library('excel');
					//activate worksheet number 1
					$this->excel->setActiveSheetIndex(0);
					//name the worksheet
					$this->excel->getActiveSheet()->setTitle('test worksheet');
					//titulos
					$this->excel->getActiveSheet()->SetCellValue("A1",'TIPO DE PRESTAMO');
					$this->excel->getActiveSheet()->SetCellValue("B1",'ESTATUS');
					$this->excel->getActiveSheet()->SetCellValue("C1",'C.I SOCIO');
					$this->excel->getActiveSheet()->SetCellValue("D1",'NOMBRE SOCIO');
					$this->excel->getActiveSheet()->SetCellValue("E1",'FECHA DE SOLICITUD');
					$this->excel->getActiveSheet()->SetCellValue("F1",'MONTO');
					
					
					//ajustamos el ancho de las columnas al ancho del texto
					$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
					
					$i=2;
					foreach($data as $result )
					{
						
						$this->excel->getActiveSheet()->SetCellValue("A".$i, $result['descripcion']);
						$this->excel->getActiveSheet()->SetCellValue("B".$i, $result['status']);
						$this->excel->getActiveSheet()->SetCellValue("C".$i, $result['cedula']);
						$this->excel->getActiveSheet()->SetCellValue("D".$i, $result['nombre']);
						$this->excel->getActiveSheet()->SetCellValue("E".$i, $result['fecha_creado']);
					    $this->excel->getActiveSheet()->SetCellValue("F".$i, $result['monto']);
						
						
						$i++;
					}
					
				 //set aligment to center for that merged cell (A1 to D1)
				  $this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				  
				   
				  $filename='tipo_prestamos.xlsx'; //save our workbook as this file name
				  header('Content-Type: application/vnd.ms-excel'); //mime type
				  header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
				  header('Cache-Control: max-age=0'); //no cache
							   
				  //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
				  //if you want to save it as .XLSX Excel 2007 format
				  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
				  //force user to download the Excel file without writing it to server's HD
				  $objWriter->save('php://output');
				
			}
			
		
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */