<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	    $this->load->model('Reportes_model');
	    
	    

	}
	
	public function lista_socios()
	{		
		$data["item"]=$this->Reportes_model->get_list_socios();
		
		
			if(empty($data["item"]))
			{ 
				$data['error']='No hay Socios Activos';
				$this->template->write_view('content', 'no_encontrado',$data,TRUE);
			}
			else
			{
						$this->template->add_css('css/demo_table.css');
						$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
						$this->template->add_js('js/datatable/1.10.4/dataTables.bootstrap.js');
						$this->template->add_js('js/funciones_datatable.js');
						$this->template->write_view('content', 'reportes/lista_socios_view',$data,TRUE);
			}
		
		
		
		
		$this->template->write('title', 'Lista de Socios', TRUE);
		$this->template->write_view('menu', $this->ion_auth->getter_menu());
	    $this->template->render();
	}
	
	
	public function lista_tipo_prestamo()
	{
		
		
		$this->template->add_css('css/demo_table.css');

		$this->template->add_css('css/jquery-ui-1.10.0.custom.css');
		$this->template->add_js('js/jquery-ui.js');
		$this->template->add_js('js/jquery.ui.datepicker-es.js');
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js/datatable/1.10.4/dataTables.bootstrap.js');
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js/funciones_datatable.js');
		$this->template->add_js('js/funciones.js');
		
		$this->template->write('title', "Reporte Tipo de Prestamos", TRUE);
		$this->template->write_view('menu', $this->ion_auth->getter_menu());
		$this->template->write_view('content', 'reportes/lista_tipo_prestamos_view',TRUE);
		$this->template->render();
		
		
	}
	
	public function get_lista_prestamo_tipo()
	{
		$fecha_inicial	=	$this->input->post("fecha_inicial");
		$fecha_final	=	$this->input->post("fecha_final");
		$id_tipo_prestamo = $this->input->post("id_tipo_prestamo");
		
			$data["item"]=$this->Reportes_model->get_list_prestamo_tipo($fecha_inicial,$fecha_final,$id_tipo_prestamo);
		
			if(empty($data["item"]))
			{ 
				$data['error']='No se encontraron registros en la base de datos, intente de nuevo';
				//$this->template->write_view('content', 'no_encontrado',$data,TRUE);
				$respuesta= $this->load->view('no_encontrado',$data,TRUE);
			}
			else
			{
				$data['fecha_inicial']		=	$fecha_inicial;
				$data['fecha_final']		=	$fecha_final;
				$data['id_tipo_prestamo']	=	$id_tipo_prestamo;
				//$this->template->write_view('content', 'reportes/lista_socios_view',$data,TRUE);
				$respuesta= $this->load->view('reportes/lista_tipo_prestamo_data',$data,TRUE);
			}
			echo $respuesta;
		
	}
	
	
	
	
	// lista estatus de Prestamos
	
	
	public function lista_estatus_prestamo()
	{
		
		
		$this->template->add_css('css/demo_table.css');

		$this->template->add_css('css/jquery-ui-1.10.0.custom.css');
		$this->template->add_js('js/jquery-ui.js');
		$this->template->add_js('js/jquery.ui.datepicker-es.js');
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js/datatable/1.10.4/dataTables.bootstrap.js');
		$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
		$this->template->add_js('js/funciones_datatable.js');
		$this->template->add_js('js/funciones.js');
		
		$this->template->write('title', "Reporte Estatus de Prestamos", TRUE);
		$this->template->write_view('menu', $this->ion_auth->getter_menu());
		$this->template->write_view('content', 'reportes/lista_estatus_prestamos_view',TRUE);
		$this->template->render();
		
		
	}
	
	public function get_lista_prestamo_estatus()
	{
		$fecha_inicial	=	$this->input->post("fecha_inicial");
		$fecha_final	=	$this->input->post("fecha_final");
		$id_estatus_prestamo = $this->input->post("id_estatus_prestamo");
		
			$data["item"]=$this->Reportes_model->get_list_prestamo_estatus($fecha_inicial,$fecha_final,$id_estatus_prestamo);
		
			if(empty($data["item"]))
			{ 
				$data['error']='No se encontraron registros en la base de datos, intente de nuevo';
				//$this->template->write_view('content', 'no_encontrado',$data,TRUE);
				$respuesta= $this->load->view('no_encontrado',$data,TRUE);
			}
			else
			{
				$data['fecha_inicial']			=	$fecha_inicial;
				$data['fecha_final']			=	$fecha_final;
				$data['id_estatus_prestamo']	=	$id_estatus_prestamo;
				//$this->template->write_view('content', 'reportes/lista_socios_view',$data,TRUE);
				$respuesta= $this->load->view('reportes/lista_estatus_prestamo_data',$data,TRUE);
			}
			echo $respuesta;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */