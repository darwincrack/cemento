<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prestamos extends CI_Controller {
var $total_haberes;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	    $this->load->model('Prestamos_model');
	    
	    

	}
	
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
				
		else
		{
			$this->template->add_js('js/angular.min.js');
			$this->template->add_js('js/app/controller/prestamos.js');
			$user_id=$this->ion_auth->get_user_id();
			$item=$this->Prestamos_model->get_haberes_user($user_id);
			
			foreach ($item as $items)
   			{
   					$this->total_haberes= $items["total_haberes"];
  							
  			}
  			
  			$data["total_haberes"]=$this->total_haberes;
  			$data["corto_plazo"]=($this->total_haberes*30)/100;
			$data["mediano_plazo"]=($this->total_haberes*50)/100;
			
			$this->template->write('title', 'Solicitud de Prestamos', TRUE);
			$this->template->write_view('menu', $this->ion_auth->getter_menu());
		    $this->template->write_view('content', 'solicitudes/prestamos_view',$data,TRUE);	
			$this->template->render();
		}
		
		
	}
	
	public function gen_solicitud()
	{
		$id_user=$this->ion_auth->get_user_id();
		$id_tipo_prestamo= $this->input->post("id_tipo_prestamo");
		$monto= $this->input->post("monto");
		
		$this->Prestamos_model->gen_solicitud($id_tipo_prestamo,$monto,$id_user);
		echo json_encode(array("respuesta" => "success"));
		
		
		
	}
	
	
		public function historial()
	{
		$id_user=$this->ion_auth->get_user_id();
		
		$data["item"]=$this->Prestamos_model->historial_prestamos($id_user);
		
		$this->template->write('title', 'Historial de Prestamos', TRUE);
		$this->template->write_view('menu', $this->ion_auth->getter_menu());
		$this->template->write_view('content', 'solicitudes/historial_view',$data,TRUE);	
	    $this->template->render();
		
		
		
		
	}
	
	
		
		public function historial_all()
	{
		
		$id_user=$this->ion_auth->get_user_id();
		
		$data["item"]=$this->Prestamos_model->historial_prestamos();
		
		
			if(empty($data["item"]))
			{ 
				$data['error']='No hay solicitudes pendientes';
				$this->template->write_view('content', 'no_encontrado',$data,TRUE);
			}
			else
			{
						$this->template->add_css('css/demo_table.css');
						$this->template->add_js('js/datatable/1.10.4/jquery.dataTables.min.js');
						$this->template->add_js('js//datatable/1.10.4/dataTables.bootstrap.js');
						$this->template->add_js('js/funciones_datatable.js');
						$this->template->add_js('js/funciones.js');
						
				
				
					$this->template->write_view('content', 'solicitudes/historial_full_view',$data,TRUE);
			}
		
		
		
		
		$this->template->write('title', 'Procesar Prestamos', TRUE);
		$this->template->write_view('menu', $this->ion_auth->getter_menu());
	    $this->template->render();
		
		
		
		
	}
	
	
	
		public function change_status()
		{
			
		$id_user=$this->ion_auth->get_user_id();
		
		$id_prestamo=$this->input->post("id_prestamo");
		$id_status_prestamo=$this->input->post("id_status_prestamo");
		
		$data["item"]=$this->Prestamos_model->change_status($id_prestamo,$id_status_prestamo,$id_user);
		
		
		}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/contacto.php */