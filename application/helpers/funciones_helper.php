<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
 
 /**
 * Retorna la ip real de usuario
 */
 
  function getRealIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
		
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	
	return $_SERVER['REMOTE_ADDR'];
}

  /**
 * desencripta una cadena de caracteres
 *@string Es la cadena a encryptar
 *@key Es la llave para encryptar 
 */
  function encrypt($string, $key='c0rr35p0nd3nc14+') {
	 $result = '';
	 for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)+ord($keychar));
		$result.=$char;
	 }
	 return base64_encode($result);
  }
  
 /**
 * desencripta una cadena de caracteres
 *@string Es la cadena a desencryptar
 *@key Es la llave para desencryptar 
 */
  function decrypt($string, $key='c0rr35p0nd3nc14+') {
	 $result = '';
	 $string = base64_decode($string);
	 for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
	 }
	 return $result;
  }
  
  
  
/**
 * limpia todos los espacios en blanco de una cadena
 *@cadena Es la cadena a quitar espacios en blanco

 */
  
  function limpia_espacios($cadena){

    $cadena = str_replace(' ', '', $cadena);
    return $cadena;
}