<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Csv_model extends CI_Model {

  
  public function get_name_csv()
  {
  	$this->db->select('id,nombre, table_database');
	$this->db->from('nombre_csv');
	$this->db->where('activo', 'TRUE');
	$query=$this->db->get();
	return $query->result_array();
  }
 
 
 
  public function set_upload_file($id_csv)
  {
  		$this->db->trans_start();
  		$data = array('activo' => 'FALSE');
		$this->db->where('id_nombre_csv', "$id_csv");
		$this->db->update('file_upload', $data); 
  			
  		
  		$data = array(
			'id_nombre_csv' => "$id_csv" ,
			'fecha' => date("Ymd") ,
			'activo' => 'TRUE'
		);
		$this->db->insert('file_upload', $data); 
		return $this->db->insert_id();
  }
  
  
  public function user($cedula, $nombre=FALSE)
  {
  		$this->db->trans_start();
  	  	$this->db->select('id');
		$this->db->from('users');
		$this->db->where('cedula', "$cedula");
		$query=$this->db->get();
		
		
		
		if ($query->num_rows() > 0)
		{
			$row=$query->row_array();
			return $row['id'];
		}
		else
		{
			$data = array(
			'first_name' => "$nombre" ,
			'cedula' => "$cedula" ,
			'active' => '0',
			'created_on' => time()
			);
			$this->db->insert('users', $data); 
			return $this->db->insert_id();
			
			
				
		}
		 
	$this->db->trans_complete();	
		
  }
  
  
  
  
  //set_edo_cuenta
   public function set_edo_cuenta($id_csv,$table_database,$result_csv)
  {
	$this->db->trans_start();
	$last_id=$this->set_upload_file($id_csv);
	
	
	foreach ($result_csv as $row)
	{
		$id_user=$this->user($row['0'],utf8_encode($row['1']));
		$total_haberes=$row['5'];
		$saldo_prestamo=$row['7'];
		$disponible=$row['6'];
		$saldo_fian=$row['8'];
		$bloqueado=$row['9'];
	    $disp_pres=$row['10'];
	    $dispo_reti_haberes=$row['11'];
	    $dispo_fian=$row['12'];
	    $aport_socio=$row['24'];
	    $aport_patron=$row['25'];
	    $data = array(
		'users_id' => "$id_user" ,
		'total_haberes' =>  "$total_haberes",
		'saldo_prestamo' => "$saldo_prestamo",
		'disponible' => "$disponible",
		'saldo_fian' => "$saldo_fian",
		'bloqueado' => "$bloqueado",
		'disp_pres' => " $disp_pres",
		'dispo_reti_haberes' => "$dispo_reti_haberes",
		'dispo_fian' => "$dispo_fian",
		'apor_socio' => " $aport_socio",
		'apor_patron' => " $aport_patron",
		'id_upload_file' => "$last_id",
		);
		$this->db->insert("$table_database", $data); 
	}
	

	$this->db->trans_complete();
	
		
  }
  
  

  public function set_concept($id_csv,$table_database,$result_csv)
  {
	$this->db->trans_start();
	$last_id=$this->set_upload_file($id_csv);
	
	$this->db->empty_table("concepto");
	foreach ($result_csv as $row)
	{
		$id=$row['1'];
		$descripcion=utf8_encode($row["2"]);
	    $data = array(
		'id' => "$id" ,
		'descripcion' =>  "$descripcion",
		'id_upload_file' => "$last_id"
		);
		$this->db->insert("$table_database", $data); 
	}
	$this->db->trans_complete();
	
}
 
  
    public function set_mov($id_csv,$table_database,$result_csv)
  {
	$this->db->trans_start();
	$last_id=$this->set_upload_file($id_csv);
	
	foreach ($result_csv as $row)
	{
		$id_user=$this->user($row['0']);
		$id_concepto=$row['1'];
		$fecha_prestamo=$row['2'];
		$saldo_deuda=$row['3'];
		$cuota_mes=$row['4'];
		$fecha_ultimo_pago=$row['5'];
	    $monto_otorgado=$row['8'];
	    $taza=$row['9'];
	    $data = array(
		'id_users' => "$id_user" ,
		'concepto_id' =>  "$id_concepto",
		'fecha_prestamo' => "$fecha_prestamo",
		'saldo_deuda' => "$saldo_deuda",
		'cuota_mes' => "$cuota_mes",
		'fecha_ultimo_pago' => "$fecha_ultimo_pago",
		'monto_otorgado' => " $monto_otorgado",

		'id_upload_file' => "$last_id",
		);
		$this->db->insert("$table_database", $data); 
	}
	

	$this->db->trans_complete();
	
		
  }
  
  
  public function set_detalles_mov($id_csv,$table_database,$result_csv)
  {
  	$this->db->trans_start();
	$last_id=$this->set_upload_file($id_csv);
	
	foreach ($result_csv as $row)
	{
		$id_user=$this->user($row['0']);
		$id_concepto=$row['1'];
		$fechamov=$row['2'];
		$desc_mov=utf8_encode($row['3']);
		$monto=$row['4'];
		$saldo=$row['5'];
	    
	    $data = array(
		'id_users' => "$id_user" ,
		'id_concepto' =>  "$id_concepto",
		'fechamov' => "$fechamov",
		'desc_mov' => "$desc_mov",
		'monto' => "$monto",
		'saldo' => "$saldo",
		'id_upload_file' => "$last_id",
		);
		$this->db->insert("$table_database", $data); 
	}
	

	$this->db->trans_complete();
	
		
  }  
  
    
   
  
  
}

/* End of file listas.php */
/* Location: .application/models/listas/listas.php */