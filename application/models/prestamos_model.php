<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Prestamos_model extends CI_Model {

  public function get_haberes_user($user_id)
  {


	$this->db->select('total_haberes,apor_socio,apor_patron, users_id');
	$this->db->from('edo_cuenta');
	$this->db->join('file_upload', 'edo_cuenta.id_upload_file = file_upload.id');
	$this->db->where('file_upload.activo', 'TRUE');
	$this->db->where('edo_cuenta.users_id', "$user_id");
	$query=$this->db->get();
	return $query->result_array();
 

  
  }
  
  
  public function gen_solicitud($id_tipo_prestamo,$monto,$id_user){
  	
  			$data = array(
			'id_tipo_prestamo' => "$id_tipo_prestamo" ,
			'monto' => "$monto" ,
			'users_id' => "$id_user",
			'fecha_creado'=>date('Ymd')
			);
			$this->db->insert('prestamos', $data); 
			$id_prestamo= $this->db->insert_id();
			
			
			 $data = array(
			'id_prestamo' => "$id_prestamo" ,
			'id_status' => "1" ,
			'activo' => "1",
			'id_user_creado_por' => "$id_user",
			
			);
			$this->db->insert('detalles_prestamo', $data);
			
			
  	
  	
  }
  
    
  public function historial_prestamos($id_user=FALSE){
  	
	if($id_user==FALSE)
	{
		$this->db->select('prestamos.id,prestamos.monto,prestamos.fecha_creado, tipo_prestamos.descripcion, status_prestamos.descripcion as status,users.first_name as nombre, users.cedula as cedula');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->join('users', 'users.id=prestamos.users_id');
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$this->db->where('detalles_prestamo.id_status', "1");
		$query=$this->db->get();	
	}
	else{
		$this->db->select('prestamos.monto, tipo_prestamos.descripcion,prestamos.fecha_creado, status_prestamos.descripcion as status');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->where('prestamos.users_id', "$id_user");
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$query=$this->db->get();	
	}
	
	return $query->result_array();
			
			
  	
  	
  }
  
  
  public function change_status($id_prestamo,$id_status,$user_id){
  	  	
  		$data = array('activo' => 'FALSE',);
		$this->db->where('id_prestamo', "$id_prestamo");
		$this->db->update('detalles_prestamo', $data);
		
		
		  		
  		$data = array(
			'id_prestamo' => "$id_prestamo" ,
			'id_status' => "$id_status" ,
			'activo' => 'TRUE',
			'id_user_creado_por' => "$user_id"
		);
		$this->db->insert('detalles_prestamo', $data); 
		
		
  }
  
  
}

/* End of file listas.php */
/* Location: .application/models/listas/listas.php */