<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Reportes_model extends CI_Model {

  public function get_list_socios()
  {

	$this->db->select('email,first_name as nombre,phone as telefono, cedula, groups.description as descrip');
	$this->db->from('users');
	$this->db->join('users_groups', 'users.id = users_groups.user_id');
	$this->db->join('groups', 'users_groups.group_id = groups.id');
	$this->db->where('register', '1');
	$this->db->where('groups.description', 'socios');
	$query=$this->db->get();
	return $query->result_array();
 
  }
  
  
  public function get_list_prestamo_tipo($fecha_inicial,$fecha_final,$id_tipo_prestamo=FALSE)
  {
  		
  		if($id_tipo_prestamo=="all")
  		{
			$this->db->select('prestamos.id,prestamos.monto,prestamos.fecha_creado, tipo_prestamos.descripcion, status_prestamos.descripcion as status,users.first_name as nombre, users.cedula as cedula');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->join('users', 'users.id=prestamos.users_id');
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$this->db->where('prestamos.fecha_creado >=', "$fecha_inicial");
		$this->db->where('prestamos.fecha_creado <=', "$fecha_final");
		$this->db->order_by("prestamos.fecha_creado", "desc");
		
		
		$query=$this->db->get();
		}
		else
		{
			$this->db->select('prestamos.id,prestamos.monto,prestamos.fecha_creado, tipo_prestamos.descripcion, status_prestamos.descripcion as status,users.first_name as nombre, users.cedula as cedula');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->join('users', 'users.id=prestamos.users_id');
		$this->db->where('prestamos.id_tipo_prestamo', "$id_tipo_prestamo");
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$this->db->where('prestamos.fecha_creado >=', "$fecha_inicial");
		$this->db->where('prestamos.fecha_creado <=', "$fecha_final");
		$this->db->order_by("prestamos.fecha_creado", "desc");
		
		
		$query=$this->db->get();
		}
  		
		return $query->result_array();	
  }
 
 
 
 public function get_list_prestamo_estatus($fecha_inicial,$fecha_final,$id_estatus_prestamo=FALSE)
  {
  		
  		if($id_estatus_prestamo=="all")
  		{
			$this->db->select('prestamos.id,prestamos.monto,prestamos.fecha_creado, tipo_prestamos.descripcion, status_prestamos.descripcion as status,users.first_name as nombre, users.cedula as cedula');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->join('users', 'users.id=prestamos.users_id');
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$this->db->where('prestamos.fecha_creado >=', "$fecha_inicial");
		$this->db->where('prestamos.fecha_creado <=', "$fecha_final");
		$this->db->order_by("prestamos.fecha_creado", "desc");
		
		
		$query=$this->db->get();
		}
		else
		{
			$this->db->select('prestamos.id,prestamos.monto,prestamos.fecha_creado, tipo_prestamos.descripcion, status_prestamos.descripcion as status,users.first_name as nombre, users.cedula as cedula');
		$this->db->from('prestamos');
		$this->db->join('detalles_prestamo', 'prestamos.id = detalles_prestamo.id_prestamo');
		$this->db->join('tipo_prestamos', 'prestamos.id_tipo_prestamo= tipo_prestamos.id');
		$this->db->join('status_prestamos', 'detalles_prestamo.id_status=status_prestamos.id');
		$this->db->join('users', 'users.id=prestamos.users_id');
		$this->db->where('status_prestamos.id', "$id_estatus_prestamo");
		$this->db->where('detalles_prestamo.activo', "TRUE");
		$this->db->where('prestamos.fecha_creado >=', "$fecha_inicial");
		$this->db->where('prestamos.fecha_creado <=', "$fecha_final");
		$this->db->order_by("prestamos.fecha_creado", "desc");
		
		
		$query=$this->db->get();
		}
  		
		return $query->result_array();	
  }
 
 
 
 
  
}
