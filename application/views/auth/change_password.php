




<div class="col-md-6 col-md-offset-3">
	<div class="panel panel-success  ">             
		<div class="panel-heading">
        	<h3 class="panel-title">Por favor introduce la Nueva Contrasena</h3>
        </div>
      	<div class="panel-body">
			<?php $attributes = array('role' => 'form');
            echo form_open('auth/change_password', $attributes);?>
            
            <fieldset>
                <?php echo $message;?>
                <div class="form-group has-error">
                    <?php echo form_input($old_password);?>
                </div>
                <div class="form-group has-error">
                    <?php echo form_input($new_password);?>
                </div>
                
                <div class="form-group has-error">
                    <?php echo form_input($new_password_confirm);?>
                </div>
	
   			<input type="submit" name="submit" value="Cambiar" class="btn btn-lg btn-danger btn-block">
 		</fieldset>
		<?php echo form_input($user_id);?>
		<?php echo form_close();?>
          </div>
      </div>
 </div>
