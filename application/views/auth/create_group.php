
<div class="col-md-6 col-md-offset-3">
                <div class="panel panel-success  ">
                 
                    <div class="panel-heading">
                        <h3 class="panel-title">Por favor introduce la información del grupo</h3>
                    </div>
                    
                    <div class="panel-body">
                  
                    
                    
<?php $attributes = array('role' => 'form');
echo form_open('auth/create_group', $attributes);?>

                            <fieldset>

                            <?php echo $message;?>
       <div class="form-group has-error">
           <input class="form-control floating-label" type="text" placeholder="Nombre de Grupo" name="group_name" value="" id="group_name"   autofocus>
      </div>
      
      <div class="form-group has-error">
           <input class="form-control floating-label" type="text" placeholder="Descripcion" name="description" value="" id="description" >
      </div>
                                

                                
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" name="submit" value="Crear Grupo" class="btn btn-lg btn-danger btn-block">
                            </fieldset>
                        </form>
                    </div>
                </div>
                </div>

