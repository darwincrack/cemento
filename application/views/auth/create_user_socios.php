<!DOCTYPE html>
<html lang="es" ng-app="App">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login-Venezolana de Cementos</title>

<!-- Bootstrap Core CSS -->
  
    <link href="<?php echo  base_url()?>css/bootstrap.min.css" rel="stylesheet">


    <!-- Material Design for Bootstrap -->
    <link href="<?php echo  base_url()?>css/roboto.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/material.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/ripples.min.css" rel="stylesheet">

    
    <link href="<?php echo  base_url()?>css/styles.css" rel="stylesheet">
        <link href="<?php echo  base_url()?>css/style_home.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="<?php echo  base_url()?>font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="back-site" ng-controller="user">
    <div class="navbar navbar-default navbar-fixed-top scroll-me barra">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('home') ?>">
                    <img class="img-responsive" src="<?php echo site_url('img/logo.png') ?>"  alt=""/>
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="<?php echo site_url('home') ?>">INICIO</a></li>
                    <li><a href="<?php echo site_url('auth') ?>">LOGIN</a></li>
                   
                </ul>
            </div>

        </div>
    </div>
<div class="container">

<div class="row">
<div class="col-md-6 col-sm-12 col-sm-offset-3">
<div class="well">


   <?php echo $message;?>

<?php $attributes = array('role' => 'form', 'class'=>'form-horizontal');
echo form_open('auth/create_user_socios', $attributes);?>

    <fieldset>
        <legend>Registrarse</legend>
        <div class="form-group has-error">
            <div class="col-lg-12">
            
             	<div class="input-group">
                	<input type="text" ng-blur="GetUser()" ng-model="cedula" class="form-control floating-label" name="cedula" value="<?php echo set_value('cedula'); ?>"  placeholder="Cedula" autofocus required>
            		  <div class="input-group-addon"><i ng-show="success" class="fa fa-check"></i> <i ng-show="loading" class="fa fa-spinner fa-pulse"></i><i ng-show="failed" class="fa fa-times"></i></div>
            	</div>
            </div>
        </div> 
       
       
       <!-- <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="text" class="form-control  floating-label" name="first_name" value="<?php echo set_value('first_name'); ?>" id="first_name" placeholder="Nombre" required>
            </div>
        </div>
        
         <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="text" class="form-control floating-label" name="last_name" value="<?php echo set_value('last_name'); ?>" id="last_name" placeholder="Apellido" required>
            </div>
        </div> -->
        
        

        
         <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="email" class="form-control floating-label" name="email" value="<?php echo set_value('email'); ?>" id="email" placeholder="Email" required>
            </div>
        </div>
        
         <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="text" class="form-control floating-label" name="phone" value="<?php echo set_value('phone'); ?>" id="phone" placeholder="Telefono">
            </div>
        </div>
    
         <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="password" class="form-control floating-label" name="password" value="" id="password" placeholder="Contrasena" required>
            </div>
        </div>
        
         <div class="form-group has-error">
            <div class="col-lg-12">
                <input type="password" class="form-control floating-label" name="password_confirm" value="" id="password_confirm"  placeholder="Confirmar Contrasena" required>
            </div>
        </div>

     <!--    <div class="form-group has-error">
            <div class="col-lg-12">
               <select class="form-control" id="select" name="sexo_id">
                    <option value="1">Masculino</option>
                    <option value="2">Femenino</option>
                </select>
            </div>
        </div> -->

         <div class="form-group ">
            <div class="col-lg-1 col-md-offset-3">
                <input type="submit" ng-disabled="!submit" class="btn btn-danger" name="submit" value="Registrarse">
            </div>
        </div>
        
        <div  class="form-group has-error" ng-show="failed">
        	<div class="col-lg-12 text-center">
        	{{mensaje}}<a target="_blank" href="<?php echo site_url('#contact') ?>"> Contactanos</a>
         	</div>
       </div> 
        
        
        

    </fieldset>
<?php echo form_close();?>
</div>
</div>
</div>
</div>

    <!-- jQuery -->
    <script src="<?php echo  base_url()?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo  base_url()?>js/bootstrap.min.js"></script>

            <!-- material js -->
    <script src="<?php echo  base_url()?>js/ripples.min.js"></script>
    <script src="<?php echo  base_url()?>js/material.min.js"></script>
     <script src="<?php echo  base_url()?>js/angular.min.js"></script>
    <script src="<?php echo  base_url()?>js/app/controller/user.js"></script>


    
    <script>$.material.init();</script>

</body>

</html>










