<div class="well col-lg-5 col-md-offset-3">


   <?php echo $message;?>



<?php $attributes = array('role' => 'form', 'class'=>'form-horizontal');
echo form_open(uri_string(), $attributes);?>

    <fieldset>
        <legend>Nueva Informacion</legend>
        <div class="form-group has-error">
            <div class="col-lg-12">
                 <?php echo form_input($first_name);?>
            </div>
        </div>
        
         <div class="form-group has-error">
            <div class="col-lg-12">
               <?php echo form_input($last_name);?>
            </div>
        </div> 
              
         <div class="form-group has-error">
            <div class="col-lg-12">
                 <?php echo form_input($phone);?>
            </div>
        </div>
    
         <div class="form-group has-error">
            <div class="col-lg-12">
            <?php echo form_input($password);?>
                
            </div>
        </div>
        
         <div class="form-group has-error">
            <div class="col-lg-12">
              <?php echo form_input($password_confirm);?>
               
            </div>
        </div>

         <div class="form-group has-error">
            <div class="col-lg-12">
               <select class="form-control" id="select" name="sexo">
                    <option value="1" <?php if($sexo=='1'){echo 'selected';} ?>>Masculino</option>
                    <option value="2" <?php if($sexo=='2'){echo 'selected';} ?>>Femenino</option>
                </select>
            </div>
        </div>


         <?php if ($this->ion_auth->is_admin()): ?>

          <h4><?php echo 'Miembro del Grupo'?></h4>
          
          <div class="form-group has-error">
           <div class="col-lg-12">
          <select class="form-control" name="groups[]">
		  <?php foreach ($groups as $group):?>
    
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= "selected";
                      break;
                      }
                  }
              ?>
                        <option value="<?php echo $group['id'];?>" <?php echo $checked ?>><?php echo htmlspecialchars($group['description'],ENT_QUOTES,'UTF-8');?></option>



              
              


          <?php endforeach?>
           </select>
              </div>
        </div>
		
      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

                 <div class="form-group ">
            <div class="col-lg-1 col-md-offset-3">
                <input type="submit" class="btn btn-danger" name="submit" value="Modificar">
            </div>
        </div>
        

    </fieldset>
<?php echo form_close();?>
</div>