<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login-Venezolana de Cementos</title>

<!-- Bootstrap Core CSS -->
  
    <link href="../css/bootstrap.min.css" rel="stylesheet">


    <!-- Material Design for Bootstrap -->
    <link href="<?php echo  base_url()?>css/roboto.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/material.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/ripples.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    
    <link href="<?php echo  base_url()?>css/styles.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/style_home.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="<?php echo  base_url()?>font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="back-site">
    <div class="navbar navbar-default navbar-fixed-top scroll-me barra">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('home') ?>">
                    <img class="img-responsive" src="<?php echo site_url('img/logo.png') ?>"  alt=""/>
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="<?php echo site_url('home') ?>">INICIO</a></li>
                    <li><a href="<?php echo site_url('auth') ?>">LOGIN</a></li>
                   
                </ul>
            </div>

        </div>
    </div>
    <div class="container" style="  margin-top: 10%;">
        <div class="row">
            <div class="col-md-5 col-sm-6 col-md-offset-4 col-sm-offset-3">
              
                <div class="panel panel-success  ">
                 
                    <div class="panel-heading">
                        <h3 class="panel-title">He Olvidado Mi Contraseña</h3>
                    </div>
                     <strong style="font-size:11px; padding-left:3%">Introduce tu Email, te Ayudaremos a Restablecer tu Contraseña</strong> 
                    <div class="panel-body">
                  
                    
                    
                    <?php $attributes = array('role' => 'form');
					echo form_open('auth/forgot_password', $attributes);?>
                        <form role="form">
                            <fieldset>

                            <?php echo $message;?>
                                <div class="form-group has-error">
                                
                                

                                    <input class="form-control floating-label" type="text" placeholder="Usuario" name="email" value="" id="email"  autofocus>
                                </div>
                                

                                
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" name="submit" value="Enviar" class="btn btn-lg btn-danger btn-block">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo  base_url()?>js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo  base_url()?>js/bootstrap.min.js"></script>
   <!-- material js -->
    <script src="<?php echo  base_url()?>js/ripples.min.js"></script>
    <script src="<?php echo  base_url()?>js/material.min.js"></script>


    
    <script>$.material.init();</script>

</body>

</html>