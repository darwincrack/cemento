
<p><?php echo anchor('auth/create_group', 'Crear Nuevo Grupo')?></p>
<table class="table table-striped table-hover ">
<tr>
	<th>Nombre</th>
	<th>Descripcion</th>
    <th>Accion</th>
</tr>
<?php foreach ($groups as $group):?>
<tr>
	<td><?php echo  $group['name'] ?></td>
	<td><?php echo $group['description'] ?></td>
    <td><?php echo anchor("auth/edit_group/".$group['id'], "<i class='mdi-editor-border-color'></i>") ;?></td>
</tr>

<?php endforeach;?>
</table>