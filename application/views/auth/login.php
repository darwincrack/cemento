<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login-Venezolana de Cementos</title>

    <!-- Bootstrap Core CSS -->
  
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design for Bootstrap -->
    <link href="<?php echo  base_url()?>css/roboto.min.css" rel="stylesheet">
    
    <link href="<?php echo  base_url()?>css/material.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/ripples.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo  base_url()?>css/styles.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/style_home.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link href="<?php echo  base_url()?>font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="back-site">
    <div class="navbar navbar-default navbar-fixed-top scroll-me barra">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('home') ?>">
                    <img class="img-responsive" src="<?php echo site_url('img/logo.png') ?>"  alt=""/>
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="<?php echo site_url('home') ?>">INICIO</a></li>
                    <li><a href="<?php echo  site_url()?>auth">LOGIN</a></li>
                   
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
			
                <h2 class="text-center" style="color:#797979">Por Favor Inicie Sesión</h2>
                <div class="login-panel panel well panel-default" style="margin-top: 30px;">
                 

                     
                    <div class="panel-body">
                  
                    
                       <?php echo $message;?>
                    <?php $attributes = array('role' => 'form');
					echo form_open('auth/login', $attributes);?>
                        <form role="form">
                            <fieldset>
                            <?php echo form_error('identity'); ?>
                          


                                <div class="form-group  has-error" style="padding-bottom:30px">
                                
                                
                                 <!-- <span class="input-group-addon">
                                      <i class="glyphicon glyphicon-user"></i>
                                  </span>-->


                                    <input class="form-control floating-label" type="text" placeholder="Usuario" name="identity" value="" id="identity"  autofocus>
                                </div>
                                <?php echo form_error('password'); ?>
                                <div class="form-group  has-error">
                                
                                   <!--<span class="input-group-addon">
            						<i class="glyphicon glyphicon-lock"></i>
          							</span>-->
                                <input class="form-control floating-label"  type="password" placeholder="Contraseña" name="password" value="" id="password">
                                </div>
                                
                                <div class="checkbox" style="padding-bottom:4%" >
                                    <label style="margin-right:48px">
                                    
                                    
                                        <input name="remember" type="checkbox" value="1" id="remember">Recordarme
                                    </label>
                                    </div> 

                                
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" name="submit" value="Iniciar Sesion" class="btn btn-lg btn-danger btn-block">								<div class="checkbox" style="padding-top:4%; text-align:center">
                                
                                <div class="col-md-4 col-sm-12 col-md-offset-1">
                                	<label><a href="create_user_socios" class="font-size-doce ">Registrarse</a></label>
                                </div>
                                 <div class="">
                                	<label ><a href="forgot_password" class="font-size-doce">Olvidaste la contrasena?</a></label>
                                </div>
                                    
                                    
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>


    <!-- material js -->
    <script src="../js/ripples.min.js"></script>
    <script src="../js/material.min.js"></script>


    
    <script>$.material.init();</script>

</body>

</html>