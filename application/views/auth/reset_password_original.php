<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login-Venezolana de Cementos</title>


<!-- Bootstrap Core CSS -->
  
    <link href="<?php echo  base_url()?>css/bootstrap.min.css" rel="stylesheet">


    <!-- Material Design for Bootstrap -->
    <link href="<?php echo  base_url()?>css/roboto.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/material.min.css" rel="stylesheet">
    <link href="<?php echo  base_url()?>css/ripples.min.css" rel="stylesheet">

    
    <link href="<?php echo  base_url()?>css/styles.css" rel="stylesheet">
   <link href="<?php echo  base_url()?>css/style_home.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="<?php echo  base_url()?>font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body class="back-site">
<div class="navbar navbar-default navbar-fixed-top scroll-me barra">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('home') ?>">
                    <img class="img-responsive" src="<?php echo site_url('img/logo.png') ?>"  alt=""/>
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="<?php echo site_url('home') ?>">INICIO</a></li>
                    <li><a href="<?php echo site_url('auth') ?>">LOGIN</a></li>
                   
                </ul>
            </div>

        </div>
    </div>
    <div class="container" style="  margin-top: 10%;"">
        <div class="row">
            <div class="col-md-5 col-md-offset-4">
                <div class="panel panel-success">
                 
                    <div class="panel-heading">
                        <h3 class="panel-title">Cambiar Contraseña</h3>
                    </div>
                     
                    <div class="panel-body">
                  

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open('auth/reset_password/' . $code);?>


                    <?php $attributes = array('role' => 'form');
					echo form_open('auth/reset_password/'  . $code, $attributes);?>
                           <fieldset>


	 <div class="form-group has-error">
     		<?php echo form_input($new_password);?>
      </div>
		
  <div class="form-group has-error">
     		<?php echo form_input($new_password_confirm);?>
      </div>       


	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	  <input type="submit" name="submit" value="Guardar" class="btn btn-lg btn-danger btn-block">
</fieldset>
<?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

    
     <!-- material js -->
    <script src="../../js/ripples.min.js"></script>
    <script src="../../js/material.min.js"></script>

    <script>$.material.init();</script>

</body>

</html>