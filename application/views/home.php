<!DOCTYPE html>
<html ng-app="App">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Caja de Ahorro Vencemos</title>
    <!-- BOOTSTRAP CORE CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- STYLE FOR OPENING IMAGE IN POPUP USING FANCYBOX-->
    <link href="css/jquery.fancybox.css" rel="stylesheet" />
      <!-- STYLE FOR MATERIAL DESIGN-->
    <link href="css/material.min.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="css/style_home.css" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top">

    <div class="navbar navbar-default navbar-fixed-top scroll-me barra">
        <!-- pass scroll-me class above a tags to starts scrolling -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png"  alt=""/>
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#header">INICIO</a></li>
                     <li><a href="#quienes_somos">QUIENES SOMOS</a></li>
                    <!--<li><a href="#works">INFORMACIONES</a></li>-->
                     <li><a href="#contact">CONTACTO</a></li>
                    <li><a href="<?php echo  base_url()?>auth" target="_blank">LOGIN</a></li>
                   
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR END  -->
    <div id="header">
        <div class="overlay">
            <div class="container">
                <div class="row scroll-me">
                <!--col-md-offset-3 col-sm-6 col-sm-offset-3-->
                    <div class="col-md-12 text-center ">

                        <h1 class="hit-the-floor" data-scroll-reveal="enter from the bottom after 0.2s">Caja de Ahorro Vencemos
                        </h1>

                        <h4 data-scroll-reveal="enter from the bottom after 0.4s" style="color:#1d00e3">el objetivo principal de este organismo está enmarcado en la calidad y excelencia, para satisfacer oportunamente con eficiencia y eficacia las necesidades de sus Asociados. 

                        </h4>
                        <a data-scroll-reveal="enter from the bottom after 0.6s" href="#quienes_somos" class="btn btn-danger set-bk-clr">Conocenos</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--HEADER SECTION END  -->

    <div id="quienes_somos">
        <div class="overlay">
            <div class="container">
                <div class="row text-center">
                 <div data-scroll-reveal="enter from the bottom after 0.2s">
                <h2><strong style="color: #347CDB;">¿QUIENES SOMOS?</strong></h2>
                
               
                <p class="text-justify">
Somos una Asociación Civil sin fines de lucro, autónoma con personalidad jurídica y patrimonio propio. Nuestra función principal es la de fomentar el ahorro, garantizando a nuestros asociados la satisfacción de sus necesidades de índole económico y social. 

El funcionamiento y administración de la Caja de Ahorros Vencemos se regirá por sus estatutos, reglamentos, las decisiones y acuerdos emanados de la asamblea de asociados, que fundamenta su organización y funcionamiento en los principios y condiciones establecidas en la Ley de Cajas de Ahorro, Fondos de Ahorro y Asociaciones de Ahorro Similares.</p>
</div>
<br>


<div class="col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.2s">
        <a class="media-left">
           <img src="img/mision.jpg" alt="" width="350px" />
        </a>
 </div>       
  <div class="col-md-8 col-sm-8" data-scroll-reveal="enter from the bottom after 0.2s" style="margin-bottom: 5%;">  
   		<div class="media-body">
             <h3 class="media-heading" style="color: #347CDB; font-weight: bold;">MISIÓN</h3>
                  <p class="text-justify"> 
                  	Fomentar e incrementar el ahorro sistemático y el estímulo a la formación de hábitos de economía y previsión social de nuestros asociados, mediante una gestión profesional, confiable, eficiente y honesta, en aras de contribuir con el mejoramiento de calidad de vida y ayudar a satisfacer sus necesidades. Para el logro de esos objetivos y en virtud de nuestra capacidad, se crearán políticas para el otorgamiento, de manera oportuna y eficiente, de diferentes tipos de préstamos que beneficien a todos nuestros asociados. 
                  </p>
        </div>
   </div>
 
 <br>
 <br>

 <div class="col-md-8 col-sm-8" data-scroll-reveal="enter from the bottom after 0.2s">
    <div class="media-body">
       <h3 class="media-heading" style="color: #347CDB; font-weight: bold;">VISIÓN</h3>
       <p class="text-justify"> 
           Ser una Asociación con una gestión transparente e innovadora, confiable y eficiente, capaz de responder estratégicamente a las expectativas de sus asociados para estar a la vanguardia de la satisfacción de sus requerimientos en pro de su bienestar y calidad de vida, dirigiendo la administración de sus recursos a la más alta rentabilidad con un personal altamente calificado, con ética y vocación de servicio y dotado de recursos suficientes y avanzadas herramientas tecnológicas. 
      </p>
    </div>
 </div>       
  <div class="col-md-4 col-sm-4" data-scroll-reveal="enter from the bottom after 0.2s">  
       <a class="media-left">
           <img src="img/vision.jpg" alt="" width="350px" />
        </a>
   </div>

                    <div class="col-md-12" data-scroll-reveal="enter from the bottom after 0.2s">
                    	<h2 style="color: #347CDB;">JUNTA DIRECTIVA</h2>
                    	La junta directiva de la Caja de Ahorros Vencemos, está conformada por:
                    	<!--Consejo de Administración-->
                    	<div data-scroll-reveal="enter from the bottom after 0.2s">
                    	<h4><strong style="color:#00BD00">Consejo de Administración</strong></h4>
                    	<br>
                    	
                    	<table class="table table-bordered table-hover">
                    	<thead style="color: #FC4949">
 							<tr>
 								<th style="text-align: center">
 									CARGO
 								</th>
 								<th style="text-align: center">
 									PRINCIPAL
 								</th>
 								<th style="text-align: center">
 									SUPLENTE
 								</th>
 							</tr>
 							</thead>
 							<tr>
 								<td>PRESIDENTE</td>
 								<td>vacante</td>
 								<td>Maria Elena Medrano Virla</td>
 							</tr>
 							<tr>
 								<td>TESORERO</td>
 								<td>Mariela Rodriguez Marcano</td>
 								<td>Nelly Marino Hernandez</td>
 							</tr>
 							<tr>
 								<td>SECRETARIO</td>
 								<td>Vacante</td>
 								<td>Vacante</td>
 							</tr>
						</table>
						</div>
						<!--fin Consejo de Administración-->
						
						
						<!--INICIO Consejo de Vigilancia-->
						<div data-scroll-reveal=" enter from the bottom after 0.2s">
						<h4><strong style="color:#00BD00">Consejo de Vigilancia</strong></h4>
                    	<br>
                    	
                    	<table class="table table-bordered table-hover">
                    	<thead style="color: #FC4949">
 							<tr>
 								<th style="text-align: center; width: 21%;">
 									CARGO
 								</th>
 								<th style="text-align: center; width: 40%;">
 									PRINCIPAL
 								</th>
 								<th style="text-align: center">
 									SUPLENTE
 								</th>
 							</tr>
 							</thead>
 							<tr>
 								<td>PRESIDENTE</td>
 								<td>Amilcar Jose Rodriguez Salazar</td>
 								<td>Vacante</td>
 							</tr>
 							<tr>
 								<td>VICEPRESIDENTE</td>
 								<td>Ninosca del Valle Bastardo Garcia</td>
 								<td>Vacante</td>
 							</tr>
 							<tr>
 								<td>SECRETARIO</td>
 								<td>Josefina del Carmen Rodriguez Gil</td>
 								<td>Vacante</td>
 							</tr>
						</table>
						</div>
						<!--FIN Consejo de Vigilancia-->
						
						
						<!--INICIO Delegados Regionales-->
						<div data-scroll-reveal="enter from the bottom after 0.2s">
						<h4><strong style="color:#00BD00">Delegados Regionales</strong></h4>
                    	<br>
                    	
                    	<table class="table table-bordered table-hover">
                    	<thead style="color: #FC4949">
 							<tr>
 								<th style="text-align: center; width: 21%;">
 									REGIÓN
 								</th>
 								<th style="text-align: center; width: 40%;">
 									PRINCIPAL
 								</th>
 								<th style="text-align: center">
 									SUPLENTE
 								</th>
 							</tr>
 							</thead>
 							<tr>
 								<td>CENTRO OCCIDENTE</td>
 								<td>Waldemar Pastor Crawther Sánchez</td>
 								<td>Vacante</td>
 							</tr>
 							<tr>
 								<td>OCCIDENTE</td>
 								<td>Luis Angel Luzardo Sánchez</td>
 								<td>Leon Augusto Neuman Andrade</td>
 							</tr>
 							<tr>
 								<td>SUR</td>
 								<td>Angel Alexandro Pino Medina</td>
 								<td>Luissandry Josefina Moffi González</td>
 							</tr>
 							<tr>
 								<td>ORIENTE</td>
 								<td>Oscar Darío Guaramaima Patiño</td>
 								<td>Pedro Marchan</td>
 							</tr>
						</table>
						</div>
						<!--FIN Delegados Regionales-->
                    </div>
                    
                    <div><h3 style="color: #347CDB; font-weight: bold;">NUESTRAS PLANTAS</h3></div>
                
                
                    <div class="col-md-12" data-scroll-reveal="enter from the bottom after 0.2s">
                        <div class="carousel slide clients-carousel" id="clients-slider">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a class="fancybox-media thumbnail" title="Región Occidente" href="img/planta_barquisimeto.jpg">
                                                <img src="img/planta_barquisimeto.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                           <a class="fancybox-media thumbnail" title="Región Sur" href="img/planta_guayana.jpg">
                                                <img src="img/planta_guayana.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                        
                                        <a class="fancybox-media thumbnail" title="Región Centro Occidente" href="img/planta_maracaibo.jpg">
                                            
                                                <img src="img/planta_maracaibo.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                        <a class="fancybox-media thumbnail" title="Region Oriente" href="img/planta_pertigalete.jpg">
                                            
                                                <img src="img/planta_pertigalete.jpg" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                               

                            </div>
                            <a data-slide="prev" href="#clients-slider" class="left carousel-control">‹</a>
                            <a data-slide="next" href="#clients-slider" class="right carousel-control">›</a>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
    <!-- QUIENES SOMOS SECTION END  -->

    <section id="contact">
        <div class="overlay">
            <div class="container">
                <div class="row text-center" data-scroll-reveal="enter from the bottom after 0.2s">
                <h2><strong>CONTACTANOS</strong></h2>
                
                    <div class="col-md-6 pad-bottom">
                        <h3><strong>DIRECCION: </strong></h3>
                        <h5>Calle Londres entre Nueva York y Trinidad, </h5>
					    <h5>Urbanización Las Mercedes, Torre Venezolana de Cementos, PB, </h5>
					   <h5>Caja de Ahorros - Caracas.</h5>
					   
					    <h3><strong>TELEFONOS: </strong></h3>
                        <h5>Llamadas externas (0212) 999.73.10 </h5>
					    <h5>Llamadas internas: 67310 </h5>
					   <h5>Fax: (0212) 999.73.18</h5>					   
					   
					   <h3><strong>HORARIO DE ATENCION: </strong></h3>
                        <h5>De lunes a jueves de 8:00 am a 12:00 m y de 1:00 pm a 5:00 pm. </h5>
					    <h5>Viernes de 8:00 am a 12:00 m </h5>
					    
					   
					    
                    </div>
                    <div class="col-md-6 pad-bottom">
                    	 <h3><strong>CORREO ELECTRONICO: </strong></h3>
                        <h5>Interno: Caja de Ahorro Asociados/VDC/CDV </h5>
					    <h5>Externo: cajadeahorroasociados@venceremos.com.ve </h5>
					    <br>
					    
					    <h3><strong>ATENCION ASEGURADOS VEHICULOS: </strong></h3>
                        <h5>Llamadas externas (0212) 999.7046 </h5>
					    <h5>Llamadas internas: 67046</h5>
					    <h5>Correo Interno: Administracion de Seguro Automotriz/VDC/CDV</h5>
					    <h5>Correo Externo: administracion.seguroautomotriz@venceremos.com.ve  </h5>
                    </div>
                </div>

                <div class="row text-center" data-scroll-reveal="enter from the bottom after 0.4s">

                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
 <a data-toggle="modal" data-target="#contact_modal" data-original-title data-scroll-reveal="enter from the bottom after 0.6s" class="btn btn-danger set-bk-clr">Envianos un mensaje</a>

                    </div>

                </div>





            </div>
        </div>
    </section>
    <!-- CONTACT SECTION END -->
    <footer>
        &copy 2015 System  | <a href="#" target="_blank">by GrupoProyecto</a>
    </footer>
        <!--FOOTER SECTION END  -->
    
    <!-- FORMULARIO DE CONTACT-->
    <div class="container" ng-controller="contactos">
	<div class="row">
        <div class="modal fade" id="contact_modal" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="background-color: #3885EA">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-info-sign"></span> ¿Alguna Pregunta? no dude en contactar con nosotros</h4>
                    </div>
                    <form action="#" method="post" accept-charset="utf-8" name="modalForm">
                    <div class="modal-body" style="padding: 5px;">
                          <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                    
                                    <div class="form-group  has-error">
 										<input ng-model="nombre" class="form-control floating-label" name="nombre" id="nombre" placeholder="Nombre" type="text" required autofocus />
 										<!--<span style="color:#FF0000; font-size: 11px;" ng-show="modalForm.nombre.$dirty && modalForm.nombre.$error.required">requerido</span>-->
 
                                	</div>

                                </div>   
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    
                                    <div class="form-group  has-error">
                                    	<input ng-model="email" class="form-control floating-label" name="email" id="email" placeholder="E-mail" type="email" required />
                                    	
                                    	<!-- <span style="color:#FF0000; font-size: 11px;" ng-show="modalForm.email.$dirty && modalForm.email.$error.required">requerido</span>
                                    	 <span style="color:#001C66; font-size: 11px;" ng-show='modalForm.email.$dirty && modalForm.email.$error.email'>Ingresa un email valido</span>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                
                                	<div class="form-group  has-error">
                                    	<textarea ng-model="mensaje" style="resize:vertical;" class="form-control floating-label" placeholder="Mensaje..." rows="6" name="mensaje" id="mensaje" required></textarea>
                                    <!--	 <span style="color:#FF0000; font-size: 11px;" ng-show="modalForm.mensaje.$dirty && modalForm.mensaje.$error.required">requerido</span>-->
                                    	
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="panel-footer" style="margin-bottom:-14px;">
                     
                            <input type="button" class="btn btn-success" value="Enviar" ng-click="InsertContacto()"/> <!--ng-disabled='!modalForm.$valid'-->
                            
								
							
                                <!--<span class="glyphicon glyphicon-ok"></span>-->
                            <input type="reset" class="btn btn-danger" value="Limpiar" />
                             <i ng-show="loading">Espere...</i>{{respuesta}}
                                <!--<span class="glyphicon glyphicon-remove"></span>-->
                            <button style="float: right;" type="button" class="btn btn-default btn-close" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
	</div>
    
    <!-- FIN FORMULARIO DE CONTACTO-->

    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  SCRIPTS -->
    <script src="js/jquery.min.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="js/bootstrap.min.js"></script>
    <!-- SCROLLING SCRIPTS PLUGIN  -->
    <script src="js/jquery.easing.min.js"></script>
    <!--  FANCYBOX PLUGIN -->
    <script src="js/jquery.fancybox.js"></script>
     <!-- SCROLL ANIMATIONS  -->
   <script src="js/scrollReveal.js"></script>
    <!-- CUSTOM SCRIPTS   -->
    <script src="js/custom.js"></script>
       <!--MATERIAL DESIGN -->
   <script src="js/material.min.js"></script>
    <script src="js/ripples.min.js"></script>
      <!-- ANGULARJS-->
    <script src="js/angular.min.js"></script>
     <script src="js/app/controller/contact.js"></script>
    
    <script>$.material.init();</script>

</body>
</html>