               <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="active">

                            <a href="#"><i class="fa fa-binoculars"></i> Gestionar Solicitudes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a  href="#">Prestamos Solicitados</a>
                                </li>
                                 <li>
                                    <a  href="#">Reporte de Prestamos Solicitados</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-newspaper-o"></i> Noticias</a>
                        </li>

                        <li>       
                            <a  href="#"><i class="fa fa-key"></i> Cambiar Contrasena</a>
                        </li>
                    </ul>
                </div>