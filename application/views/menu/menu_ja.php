               <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>       
                            <a  href="#"><i class="fa fa-newspaper-o"></i> Noticias</a>
                        </li>

                        <li class="active">
                            <a href="#"><i class="fa fa-bar-chart"></i> Estadisticas <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Lista de Socios</a>
                                </li>
                                <li>
                                    <a href="#">Lista de Prestamos Solicitados</a>
                                </li>
                                 <li>
                                    <a href="#">Reporte por tipo de prestamo </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-key"></i> Cambiar Contrasena</a>
                        </li>
                    </ul>
                </div>