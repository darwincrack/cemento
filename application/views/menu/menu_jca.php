               <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                          <li>
                            <a href="#"><i class="mdi-social-person"></i> Usuarios <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url('auth/administrar_user')  ?>">Administrar</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('auth/groups')  ?>">Grupos </a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>

                            <a href="#"><i class="fa fa-binoculars"></i> Gestionar Solicitudes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a  href="<?php echo site_url('solicitudes/prestamos/historial_all')  ?>">Prestamos Solicitados</a>
                                </li>
                               <li>
                                    <a  href="#">Reporte de Prestamos Solicitados</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-newspaper-o"></i> Noticias</a>
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-cogs"></i> Administrar Noticias</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart"></i> Estadisticas <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Lista de Socios</a>
                                </li>
                                <li>
                                    <a href="#">Lista de Prestamos Solicitados</a>
                                </li>
                                 <li>
                                    <a href="#">Reporte por tipo de prestamo </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-key"></i> Cambiar Contrasena</a>
                        </li>
                    </ul>
                </div>