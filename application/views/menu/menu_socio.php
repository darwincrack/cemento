               <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       <li class="active">

                            <a href="#"><i class="fa fa-usd"></i> Prestamos <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo site_url('solicitudes/prestamos')  ?>">Solicitar</a>
                                </li>
                                <li>
                                    <a href="#">Estado de Cuenta </a>
                                </li>
                                 <li>
                                    <a href="<?php echo site_url('solicitudes/prestamos/historial')  ?>">Historial de prestamos </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>       
                            <a  href="#"><i class="fa fa-newspaper-o"></i> Noticias</a>
                        </li>

                        <li>       
                            <a  href="#"><i class="fa fa-key"></i> Cambiar Contrasena</a>
                        </li>
                    </ul>
                </div>