<div class="alert alert-danger text-center" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Atencion:</span>
 <?php echo "$error" ?> 
</div>