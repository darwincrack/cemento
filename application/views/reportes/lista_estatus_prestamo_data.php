
<style>
	.dataTables_filter{
		text-align: right;
	}
	#datatable_paginate
	{
		text-align: right;
	}
</style>

<?php	echo anchor(base_url()."excel/export_excel/lista_prestamo_estatus/".str_replace("/","_",$fecha_inicial).'/'.str_replace("/","_",$fecha_final).'/'.$id_estatus_prestamo,"Descargar <i class='fa fa-file-excel-o'></i>");?>
<div>
<table class="table table-striped table-hover text-center" id="datatable">
<thead>
<tr>
	<th class="text-center">Tipo de Prestamo</th>
	<th >estatus</th>
	<th class="text-center">C.I. Socio</th>
	<th class="text-center">Nombre Socio</th>
	<th class="text-center">Fecha de Solicitud</th>
	<th class="text-center">Monto</th>

	
	
</tr>
</thead>
<?php foreach ($item as $items):?>
<tr>
	<td><?php echo strtoupper($items['descripcion']) ?></td>
	<td><?php echo $items['status'] ?></td>
	<td><?php echo strtoupper($items['cedula']) ?></td>
	<td><?php echo $items['nombre'] ?></td>
    <td><?php echo $items['fecha_creado'] ?></td>
	<td><?php echo number_format($items['monto'],2,',','.') ?></td>
	
</tr>

<?php endforeach;?>
</table>
</div>