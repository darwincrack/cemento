
<style>
	.dataTables_filter{
		text-align: right;
	}
	#datatable_paginate
	{
		text-align: right;
	}
</style>

<div style="text-align: center">
<?php	echo anchor(site_url()."excel/export_excel/lista_socios","Descargar en excel <i class='fa fa-file-excel-o'></i>");?>
</div>
<div>
<table class="table table-striped table-hover text-center" id="datatable">
<thead>
<tr>
	<th class="text-center">Cedula</th>	
	<th class="text-center">Nombre</th>
	<th class="text-center">email</th>
	<th class="text-center">telefono</th>
	
</tr>
</thead>
<?php foreach ($item as $items):?>
<tr>
	<td><?php echo $items['cedula'] ?></td>
	<td><?php echo strtoupper($items['nombre']) ?></td>
	<td><?php echo $items['email'] ?></td>
    <td><?php echo $items['telefono'] ?></td>
</tr>

<?php endforeach;?>
</table>
</div>