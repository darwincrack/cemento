<div class="col-md-10 col-md-offset-1">
<form class="form-inline">

  <div class="form-group">
        <div class="controls">
            <div class="input-group">
  
		 <select class="form-control" id="tipo_prestamo" name="tipo_prestamo">
    			<option value="">Seleccione tipo de Prestamo</option>
    			<option value="1">Corto Plazo</option>
    			<option value="2">Mediano Plazo</option>
    			<option value="all">Ambos</option>
           </select>
               
            </div>
        </div>
 </div>


  <div class="form-group col-md-offset-1">
        <div class="controls">
            <div class="input-group">
                <input id="date-picker-1" type="text"  class="date-picker form-control"  placeholder="Fecha Inicial"  required/>
                <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
        </div>
 </div>
  <div class="form-group">
        <div class="controls">
            <div class="input-group">
                <input id="date-picker-2" type="text" class="date-picker form-control" placeholder="Fecha Final"  required/>
                <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
        </div>
 </div>
 
  <button type="button" class="btn btn-danger" id="btn-enviar"><i class="fa fa-search"></i></button>
</form>
</div>
<br>
<br>
<br>
<hr />
<div id="grid-data"></div>