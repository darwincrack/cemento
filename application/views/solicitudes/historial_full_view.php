
<style>
	.dataTables_filter{
		text-align: right;
	}
	#datatable_paginate
	{
		text-align: right;
	}
</style>
<div ng-app="App" ng-controller="change_status">
<table class="table table-striped table-hover text-center" id="datatable">
<thead>
<tr>
	<th style="width: 50px">id</th>	
	<th class="text-center">Tipo de Prestamo</th>
	<th class="text-center">C.I. Socio</th>
	<th class="text-center">Nombre Socio</th>
	<th class="text-center">Fecha de Solicitud</th>
	<th class="text-center">Monto</th>

	<th >cambiar estatus</th>
	
</tr>
</thead>
<?php foreach ($item as $items):?>
<tr>
<td id="<?php echo 'presid_'.$items['id'] ?>"><?php echo $items['id'] ?></td>
	<td><?php echo strtoupper($items['descripcion']) ?></td>
	<td><?php echo strtoupper($items['cedula']) ?></td>
	<td><?php echo $items['nombre'] ?></td>
    <td><?php echo $items['fecha_creado'] ?></td>
	<td><?php echo number_format($items['monto'],2,',','.') ?></td>
    
    <td>
    
     
        <div class="form-group has-error">
        <div class="col-md-7">
          <select class="form-control" id="gato" name="tipo_prestamo">
    			<option value="">Seleccione</option>
    			<option value="3">APROBADO</option>
    			<option value="4">RECHAZADO</option>
           </select>
           </div>
           <div class="col-md-1">
           		<a title="enviar" style="cursor: pointer" onclick="change_status(<?php echo $items['id'] ?>)"><i class="fa fa-cogs"></i></a>
            
          </div>
       </div>
      
       
    </td>
</tr>

<?php endforeach;?>
</table>
</div>