angular.module("App",[])

.controller("contactos",function($scope,$http,$timeout){
	
		$scope.InsertContacto=function(){
			$scope.loading=true;
			$http({
                url: 'contacto/set_contact',
                method: "POST",
                data : "nombre="+$scope.nombre+"&mensaje="+$scope.mensaje+"&email="+$scope.email,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })


			.success(function(data,status,headers,config){
				$scope.respuesta=data.respuesta;
				
				if(data.respuesta=="success")
				{
					$scope.respuesta="Mensaje Enviado con exito";
					$scope.clear();
				}
				else if(data.respuesta=="failed")
				{
					$scope.respuesta="Ha Ocurrido un Error al Guardar"; 	
				}
				else if(data.respuesta=="incomplete_form")
				{
					$scope.respuesta="Datos Incorrectos"; 	
				}
				
				$scope.loading=false;
				$timeout( function(){ $scope.clear_mensaje(); }, 3000);
				
			})
			
			.error(function(error,status,headers,config){
				alert(error);
			});
		}
		
		$scope.clear=function(){
			$scope.nombre	=	null;
			$scope.email	=	null;
			$scope.mensaje	=	null;
		}
		
		$scope.clear_mensaje=function(){
			$scope.respuesta = null;
		}
		
		
});
