angular.module("App",[])
.controller("uploadftp",function($scope,$http,$timeout){
	
		$scope.csv_bd=function(){
			$scope.loading=true;
			$http({
                url: '../csv/csv/set_csv',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })


			.success(function(data,status,headers,config){
				
				$scope.respuesta="Enviado satisfactoriamente";

				$scope.loading=false;
				
			})
			
			.error(function(error,status,headers,config){
				alert(error);
			});
			
			$timeout( function(){ $scope.clear_mensaje(); }, 3000);
		}
		
		$scope.clear_mensaje=function(){
			$scope.respuesta = null;
		}
		
});
