angular.module("App",[])
.controller("solicitud_prestamo",function($scope,$http,$timeout){
	/*var corto_plazo=$("#corto_plazo").html();
	var mediano_plazo=$("#mediano_plazo").html();
	var monto =$("#monto").val();*/

		$scope.gen_solicitud=function(){
			
			$scope.loading=true;
			$http({
                url: '../solicitudes/prestamos/gen_solicitud',
                method: "POST",
                 data : "id_tipo_prestamo="+$scope.id_tipo_prestamo+"&monto="+$scope.monto,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })


			.success(function(data,status,headers,config){
				
				if(data.respuesta="success"){
					$scope.respuesta="Mensaje Enviado con exito";	
				}
				

				$scope.loading=false;
				
			})
			
			.error(function(error,status,headers,config){
				alert(error);
			});
			
			$timeout( function(){ $scope.clear_mensaje(); }, 3000);
		}
		
		$scope.clear_mensaje=function(){
			$scope.respuesta = null;
		}
		
});