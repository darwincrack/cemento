//cambiar estatus prestamos
function change_status(id)
{
	var parametros = {
		  "id_prestamo":document.getElementById("presid_"+id).innerHTML,
		  "id_status_prestamo": $("#gato").val()
		  };
	  
		  $.ajax({
		  data:  parametros,
		  url:   '../prestamos/change_status',
		  type:  'post',

		  success:  function (data) {

			  
			  alert("Procesado con exito");
			  location.reload();
			     
		  },
		  error: function(xhr, status, error) {
			  // handle error	
			 alert("Inesperado "+status+": "+error);;
		  }
	  	});
}

$(document).ready(function(){ //inicio jquery


if ($('.date-picker').length){
 $(".date-picker").datepicker();	
$(".date-picker").keypress(function(event) {event.preventDefault();});
}








//lista_tipo_prestamo



 $(document).on('click', '#btn-enviar',function (){
				
		//////////////////////////////////////////////////////////
		
			var parametros = {
			"fecha_inicial" 	: $("#date-picker-1").val(),
			"fecha_final" 		:  $("#date-picker-2").val(),
			"id_tipo_prestamo"  :  $("#tipo_prestamo").val()
			};
		
			$.ajax({
			data:  parametros,
			url:   '../reportes/get_lista_prestamo_tipo',
			type:  'post',
		    beforeSend: function () {
				  $("#grid-data").html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x "></i></div');
			},
			success:  function (data) 
			{
				  $("#grid-data").html(data);
				  $('#datatable').dataTable({
				  	
				  	   "language": {
                "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
               },
	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	"order": [[ 0, "desc" ]]
	
    
				  	
				  	
				  });
			},
			error: function(xhr, status, error) 
			{
				alert("Inesperado "+status+": "+error);
			}
		});
			
 });




//lista_estatus_prestamo



 $(document).on('click', '#btn-status',function (){
				
		//////////////////////////////////////////////////////////
		
			var parametros = {
			"fecha_inicial" 	: $("#date-picker-1").val(),
			"fecha_final" 		:  $("#date-picker-2").val(),
			"id_estatus_prestamo"  :  $("#tipo_prestamo").val()
			};
		
			$.ajax({
			data:  parametros,
			url:   '../reportes/get_lista_prestamo_estatus',
			type:  'post',
		    beforeSend: function () {
				  $("#grid-data").html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x "></i></div');
			},
			success:  function (data) 
			{
				  $("#grid-data").html(data);
				  $('#datatable').dataTable({
				  	
				  	   "language": {
                "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
               },
	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	"order": [[ 0, "desc" ]]
	
    
				  	
				  	
				  });
			},
			error: function(xhr, status, error) 
			{
				alert("Inesperado "+status+": "+error);
			}
		});
			
 });













   });//fin jquery